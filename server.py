# coding: utf-8

from datetime import datetime, timedelta
from flask import Flask
from flask import session, request, url_for, flash, \
    render_template, redirect, jsonify
from werkzeug.security import gen_salt
from flask_oauthlib.provider import OAuth2Provider

from models import db
from models import User, Client, Grant, Token


app = Flask(__name__, template_folder='templates')
app.debug = True
app.secret_key = 'c0derunner'
app.config.update({
    'SQLALCHEMY_DATABASE_URI': 'sqlite:///db.cr',
    'SQLALCHEMY_TRACK_MODIFICATIONS': True,
})
db.init_app(app)
oauth = OAuth2Provider(app)


def current_user():
    if 'id' in session:
        uid = session['id']
        return User.query.get(uid)
    return None


@app.route('/client')
def client():
    user = current_user()
    if not user:
        return redirect('/')
    item = Client(
        client_id=gen_salt(40),
        client_secret=gen_salt(50),
        _redirect_uris=' '.join([
            'http://localhost:8000/authorized',
            'http://127.0.0.1:8000/authorized',
            'http://127.0.1:8000/authorized',
            'http://127.1:8000/authorized',
            ]),
        _default_scopes='email',
        user_id=user.id,
    )
    db.session.add(item)
    db.session.commit()
    return jsonify(
        client_id=item.client_id,
        client_secret=item.client_secret,
    )


@oauth.clientgetter
def load_client(client_id):
    return Client.query.filter_by(client_id=client_id).first()


@oauth.grantgetter
def load_grant(client_id, code):
    return Grant.query.filter_by(client_id=client_id, code=code).first()


@oauth.grantsetter
def save_grant(client_id, code, request, *args, **kwargs):
    # decide the expires time yourself
    expires = datetime.utcnow() + timedelta(seconds=100)
    grant = Grant(
        client_id=client_id,
        code=code['code'],
        redirect_uri=request.redirect_uri,
        _scopes=' '.join(request.scopes),
        user=current_user(),
        expires=expires
    )
    db.session.add(grant)
    db.session.commit()
    return grant


@oauth.tokengetter
def load_token(access_token=None, refresh_token=None):
    if access_token:
        return Token.query.filter_by(access_token=access_token).first()
    elif refresh_token:
        return Token.query.filter_by(refresh_token=refresh_token).first()


@oauth.tokensetter
def save_token(token, request, *args, **kwargs):
    toks = Token.query.filter_by(
        client_id=request.client.client_id,
        user_id=request.user.id
    )
    # make sure that every client has only one token connected to a user
    for t in toks:
        db.session.delete(t)

    expires_in = token.pop('expires_in')
    expires = datetime.utcnow() + timedelta(seconds=expires_in)

    tok = Token(
        access_token=token['access_token'],
        refresh_token=token['refresh_token'],
        token_type=token['token_type'],
        _scopes=token['scope'],
        expires=expires,
        client_id=request.client.client_id,
        user_id=request.user.id,
    )
    db.session.add(tok)
    db.session.commit()
    return tok


@app.route('/oauth/token', methods=['GET', 'POST'])
@oauth.token_handler
def access_token():
    return None


@app.route('/oauth/authorize', methods=['GET', 'POST'])
@oauth.authorize_handler
def authorize(*args, **kwargs):
    user = current_user()
    if not user:
        return redirect(url_for('login'))

    # if user is authorized return True
    return True


@app.route('/api/me')
@oauth.require_oauth()
def me():
    user = request.oauth.user
    return jsonify(username=user.name, email=user.email)


"""
Server authorization page
"""
@app.route('/login', methods=('GET', 'POST'))
def login():
    if 'id' in session:
        return redirect('http://127.0.0.1:8000/')

    if request.method == 'POST':
        email = request.form.get('email', '')
        password = request.form.get('password', '')
        user = User.query.filter_by(email=email).first()

        if user and user.check_password(password):
            session['id'] = user.id
        else:
            flash(u'User does not exist or the password is incorrect', 'error')
            
        return redirect(url_for('login'))

    return render_template('login.html')


"""
Server registration page
"""
@app.route('/register')
def register():
    return render_template('register.html')


from jinja_filters import message_alert_glyph, messages_alert_tags
app.jinja_env.filters['glyph_class'] = message_alert_glyph
app.jinja_env.filters['tag_class'] = messages_alert_tags


if __name__ == '__main__':
    with app.app_context():
        db.create_all()
        app.run()