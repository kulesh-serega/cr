from flask import Flask, render_template, url_for, redirect, \
    session, request, jsonify
from flask_oauthlib.client import OAuth
from settings import CLIENT_ID, CLIENT_SECRET, ACCESS_TOKEN_URL, \
    AUTHORIZE_URL, BASE_API_URL


app = Flask(__name__)
app.debug = True
app.secret_key = 'c0derunner'
oauth = OAuth(app)

remote = oauth.remote_app(
    'remote',
    consumer_key=CLIENT_ID,
    consumer_secret=CLIENT_SECRET,
    request_token_params={'scope': 'email'},
    base_url=BASE_API_URL,
    request_token_url=None,
    access_token_url=ACCESS_TOKEN_URL,
    authorize_url=AUTHORIZE_URL,
)


@app.route('/me')
def user_get():
    if 'remote_oauth' in session:
        resp = remote.get('me')
        try:
            return jsonify(resp.data)
        except TypeError:
            return resp.data

    return redirect(url_for('index'))


@app.route('/authorized')
def authorized():
    resp = remote.authorized_response()
    if resp is None:
        return 'Access denied: reason=%s error=%s' % (
            request.args.get('error_reason', None),
            request.args.get('error_description', None)
        )

    session['remote_oauth'] = (resp['access_token'], '')
    return redirect(url_for('index'))


@remote.tokengetter
def get_oauth_token():
    return session.get('remote_oauth')


"""
Flask application part
"""
@app.route('/')
def index():
    if 'remote_oauth' in session:
        oauth_token = session['remote_oauth']
        print('Authorized')
        return render_template('index.html', access_token=oauth_token)

    next_url = None #request.args.get('next') or request.referrer or None
    print('next_url:', next_url)
    # if user is not authorized, redirect him to server login page
    return remote.authorize(
        callback=url_for('authorized', next=next_url, _external=True)
    )


if __name__ == '__main__':
    import os
    os.environ['DEBUG'] = 'true'
    os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = 'true'
    app.run(host='localhost', port=8000)